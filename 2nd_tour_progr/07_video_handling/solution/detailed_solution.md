Простая обработка видео-потока
====

Цель задачи "Простая обработка видео-потока" - познакомить старшеклассников с тем, как из программ работать с видео файлами, какие простые операции доступны над отдельными кадрами.

Задача предлагает написать программу для определения, сколько раз в видео cменились фотографии с изображением лиц разных людей.

Очевидно, что задача будет сводится к выяснению, отличается ли изображение в каком-то кадре от того, что было в предыдущем кадре.

Можно сравнивать кадры так, как предложено в задаче - с помошью Microsoft Face API.

Но в данном разборе мы рассмотрим способ, который не требует обращения к внешнему сервису.

Поскольку в видео используется сжатие, то некоторые соседние кадры будут побайтово отличаться, даже если визуально они выглядят одинаково.

Это и есть основная сложность в сравнении кадров в видео. 

Следовательно, мы не можем использовать стандартные методы сравнения больших объемов данных.

Ни побайтовое сравнение, ни сравнение криптостойких хэшей данных здесь не поможет.

Однако, исследователями были предложены другие методы определения отличия фотографий. Например, можно использовать перцептивные хэш-алгоритмы.

Перцептивные хэши, судя из их названия, принимают во внимание некоторые особенности восприятия изображений человеком.

Например, мы можем говорить, что два изображения похожи, если при сравнении областей изображения относительная яркость в них совпадает. 

Так видит далекие предметы очень близорукий человек.

Прежде чем, реализовывать свой перцептивный хэш, давайте научимся считывать отдельные кадры из видео файла.

Это делается очень просто, если использовать библиотеку OpenCV.

<**При показе экрана с кодом. Голос за кадром**>После запуска этого кода в переменной image будет сохранен первый кадр изображения. Чтобы получить следующий кадр, нужно вызвать функцию read еще раз. Обратите внимание, что в конце выводится размер одного кадра в ширину и в высоту.

```python
import cv2
vidcap = cv2.VideoCapture('1.mp4')
success, image = vidcap.read()
print(len(image), len(image[0]))
```

Вместо того, чтобы разбивать изображение на области, можно уменьшить его до такой степени, что каждый пиксель будет определять усредненное значение какого-то фрагмента изображения.

<**В конце показа экрана с кодом. Голос за кадром**>Последней командой в данном коде выводятся RGB компоненты пикселя с координатами (0,0).

```python
resized = cv2.resize(image, (12, 12))
print(len(resized), len(resized[0]))
print(resized[0][0])
```

При этом, можно сразу же перевести изображение из цветного в градации серого.

Тогда мы будем иметь дело не с яркостью каждой отдельной цветовой составляющей пикселя, а со средним значением этих яркостей.

```python
resized_grey = cv2.cvtColor(resized, cv2.COLOR_BGR2GRAY)
print(len(resized_grey), len(resized_grey[0]))
print(resized_grey[0][0])
```

Относительную яркость будем расчитывать от среднего значения по всем пикселям, которое можно определить с помощью библиотеки NumPy.

```python
import numpy as np
pixels = np.asarray(resized_grey)
average = pixels.mean()
print(average)
```

Теперь можно сказать, яркость каких пикселей выше этого среднего.

```python
difference = pixels > average
print(difference)
```

Как видно из вывода содержимого списка на экран, у нас есть двеннадцать двеннадцатибитных слов.

И мы можем использовать любой из способов для записи их в виде одного числа, которое и будет результатом хэширования исходного изображения.

```python
s = 0
for i, v in enumerate(difference.flatten()):
    if v:
        s = s + (2 ** (i % 12))
print(s)
```

Через такую операцию хэширования можно прогонять каждый кадр в видео.

<**При показе экрана с кодом. Голос за кадром**>Если для двух разных кадров хэши совпадают, то кадры одинаковые. Как только хэши отличаются, можно увеличить счетчик смены кадров на единицу.

```python
def average_hash(image):
    resized_grey = cv2.cvtColor(cv2.resize(image, (12, 12)), cv2.COLOR_BGR2GRAY)
    pixels = np.asarray(resized_grey)
    average = pixels.mean()
    difference = pixels > average
    return sum([2 ** (i % 12) for i, v in enumerate(difference.flatten()) if v])

vidcap = cv2.VideoCapture('1.mp4')
success, image = vidcap.read()
count = 0
prev_hash = average_hash(image)
while success:
    new_hash = average_hash(image)
    if prev_hash != new_hash:
        count += 1
    prev_hash = new_hash
    success, image = vidcap.read()
print(count)
```

После анализа всех кадров, значение счетчика даст искомый ответ.