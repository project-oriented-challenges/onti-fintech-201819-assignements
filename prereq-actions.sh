#!/bin/bash

echo "Prepare a TeX file with source code"
cd team_final
./prepare_sources.sh | tee src/solution.tex
cd ..
